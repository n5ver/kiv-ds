VAGRANTFILE_API_VERSION = "2"

cluster = {
  "bank-1" => { :ip => "10.0.1.11", :cpus => 1, :mem => 1024 },
}
 
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    config.vm.synced_folder ".", "/vagrant"
    config.vm.box = "geerlingguy/centos7"
    config.ssh.insert_key = false

    config.vm.define "sequencer" do |sequencer|
        sequencer.vm.network "private_network", ip: "10.0.1.10"
        sequencer.vm.hostname = "sequencer"

        sequencer.vm.provision "shell", inline: <<-SHELL
            yum -q -y install epel-release
            yum -q -y install mc
            yum -q -y install screen
            yum -q -y install git gcc zlib-devel bzip2-devel readline-devel sqlite-devel openssl-devel
            yum -q -y install python36
            pip3 install --upgrade
            pip3 install Flask
            pip3 install requests
            cp /vagrant/sequencer/sequencer.service /etc/systemd/system
            systemctl daemon-reload
            systemctl enable sequencer
            systemctl start sequencer
        SHELL
    end

    cluster.each_with_index do |(hostname, info), index|

        config.vm.define hostname do |cfg|
            cfg.vm.provider :virtualbox do |vb, override|
                override.vm.network :private_network, ip: "#{info[:ip]}"
                override.vm.hostname = hostname
                vb.name = hostname
                vb.customize ["modifyvm", :id, "--memory", info[:mem], "--cpus", info[:cpus], "--hwvirtex", "on"]

                config.vm.provision "shell", inline: <<-SHELL
                    yum -q -y install epel-release
                    yum -q -y install mc
                    yum -q -y install screen
                    yum -q -y install git gcc zlib-devel bzip2-devel readline-devel sqlite-devel openssl-devel
                    yum -q -y install python3
                    pip3 install --upgrade
                    pip3 install Flask
                    [ -f /vagrant/sequencer/servers.txt ] && echo "#{info[:ip]}":5000 > /vagrant/sequencer/servers.txt || echo "#{info[:ip]}" >> /vagrant/sequencer/servers.txt
                    echo "\n" >> /vagrant/sequencer/servers.txt
                    cp /vagrant/server/banking-server.service /etc/systemd/system
                    systemctl daemon-reload
                    systemctl enable banking-server
                    systemctl start banking-server
                SHELL

            end # end provider
        end # end config
    end # end cluster
end
#
# EOF
#