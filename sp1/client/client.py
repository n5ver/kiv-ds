import requests
import sys
import random as rnd


curr_amount = 0
ip = "10.0.1.10:5000"


def main():
    if len(sys.argv) == 2 and sys.argv[1] == "auto":
        run_automatic()
    elif len(sys.argv) == 2 and sys.argv[1] == "int":
        run_interactive()
    else:
        print_help()

    print("Quitting...")


def run_interactive():
    global curr_amount
    inp = ""

    while True:
        print_menu()
        inp = input("> ")

        try:
            arr = inp.split()
            command = int(arr[0])
            inp = command
            if command == 1 or command == 2:
                curr_amount = int(arr[1])
            if command > 6 or command < 1:
                continue
        except ValueError:
            continue
        except IndexError:
            continue

        if inp == 6:
            break

        func = switcher(command)
        func()


def run_automatic():
    global curr_amount

    for i in range(2):
        for j in range(10):
            curr_amount = rnd.randint(10000, 50000)
            if rnd.random() < 0.5:
                print("{0} - {1}".format("credit", curr_amount))
                credit()
            else:
                print("{0} - {1}".format("debit", curr_amount))
                debit()
        commit()


def print_help():
    print("Run as:")
    print("client.py int - for interactive mode")
    print("client.py auto - for automatic mode")


def print_menu():
    print("Credit (1), Debit (2), Balance (3), Commit (4), Rollback (5), Quit (6)")


def switcher(command):
    return {
        1: credit,
        2: debit,
        3: balance,
        4: commit,
        5: rollback,
    }[command]


def credit():
    global ip
    global curr_amount
    r = requests.post("http://{0}/credit".format(ip), json={"amount": curr_amount})
    print_simple_result(r.json())


def debit():
    global ip
    global curr_amount
    r = requests.post("http://{0}/debit".format(ip), json={"amount": curr_amount})
    print_simple_result(r.json())


def balance():
    global ip
    r = requests.get("http://{0}/balance".format(ip))
    print_balance(r.json())


def commit():
    global ip
    r = requests.get("http://{0}/commit".format(ip))
    if r.status_code == 200:
        print_balance(r.json())
    if r.status_code == 409:
        print_simple_result(r.json())
    if r.status_code == 400:
        print_commit_bad_request(r.json())


def rollback():
    global ip
    r = requests.get("http://{0}/rollback".format(ip))
    print_fail_success(r.json())


def print_balance(data):
    print("Balance:")
    for i in range(len(data["success"])):
        print("\t{0} - {1}".format(data["success"][i]["server"], data["success"][i]["balance"]))
    print("Fail:")
    for i in range(len(data["fail"])):
        print("\t{0} - {1}".format(data["fail"][i]["server"], data["fail"][i]["description"]))


def print_fail_success(data):
    print("Balance:")
    for i in range(len(data["success"])):
        print("\t{0} - {1}".format(data["success"][i]["server"], data["success"][i]["description"]))
    print("Fail:")
    for i in range(len(data["fail"])):
        print("\t{0} - {1}".format(data["fail"][i]["server"], data["fail"][i]["description"]))


def print_simple_result(data):
    print(data["description"])


def print_commit_bad_request(data):
    for i in range(len(data)):
        print("{0} - {1}".format(data[i]["server"], data[i]["description"]))
        if "operation" in data[i]:
            print("\toperation:")
            print("\t\ttransaction_id - {0}".format(data[i]["operation"]["transaction_id"]))
            print("\t\tamount - {0}".format(data[i]["operation"]["amount"]))
            print("\t\toperation - {0}".format(data[i]["operation"]["operation"]))


if __name__ == '__main__':
    main()
