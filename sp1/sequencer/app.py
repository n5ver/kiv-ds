import requests
from BankingOperation import *
import re
import os
import signal
import sys
import random as rnd

id_counter = 0
operations = []
servers = []
waiting_for_rollback = False


"""
error = "Next to sequencer has to be file with servers IPs. Each IP has to be on separate line."
regex = re.compile(
    r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+')
try:
    fp = open('./servers.txt', 'r')
    line = fp.readline()
    while line:
        if not regex.fullmatch(line):
            print(error)
            os.kill(os.getpid(), signal.SIGTERM)

        servers.append(line)
        line = fp.readline()
    fp.close()
except:
    print(error)
"""
print(sys.argv)
servers.append("10.0.1.11:5000")
servers.append("10.0.1.12:5000")
servers.append("10.0.1.13:5000")

from flask import Flask, request, Response, json

app = Flask(__name__)


def send_get_request(operation):
    global servers

    rnd.shuffle(servers)
    fail = []
    success = []

    for s in servers:
        message = {}
        try:
            r = requests.get("http://{0}/{1}".format(s, operation))
        except requests.exceptions.RequestException:
            message["server"] = s
            message["description"] = "Server unreachable."
            fail.append(message)
            continue

        message = r.json()
        message["server"] = s
        print("{0} - {1}".format(s, message))
        if r.status_code != 200:
            fail.append(message)
        else:
            success.append(message)

    return Response(response=json.dumps({"success": success, "fail": fail}), status=200, mimetype='application/json')


def parse_operations(req, banking_operation):
    if req.is_json:
        amount = None
        message = None
        try:
            amount = int(req.json["amount"])
            if amount < 10000 or amount > 50000:
                message = {'description': "Bad request. Value amount has to be from 10000 to 50000."}
        except KeyError:
            message = {'description': "Bad request. Value amount has to be present."}
        except:
            message = {'description': "Bad request. Value amount has to be integer."}

        if message is None:
            global operations
            global id_counter
            operations.append(BankingOperation(id_counter, banking_operation, amount))
            id_counter += 1
            return Response(response=json.dumps({'description': 'OK'}), status=200, mimetype='application/json')
        else:
            return Response(response=json.dumps(message), status=400, mimetype='application/json')

    else:
        message = {'description': "Bad request. Request has to be in JSON format."}
        return Response(response=json.dumps(message), status=400, mimetype='application/json')


@app.route('/credit', methods=['POST'])
def credit():
    return parse_operations(request, Operation.credit)


@app.route('/debit', methods=['POST'])
def debit():
    return parse_operations(request, Operation.debit)


@app.route('/balance', methods=['GET'])
def balance():
    return send_get_request("balance")


@app.route('/rollback', methods=['GET'])
def rollback():
    global waiting_for_rollback
    global operations
    waiting_for_rollback = False
    operations = []
    return send_get_request("rollback")


def test_connection():
    responses = []
    code = 200
    for s in servers:
        message = {}
        message["server"] = s
        try:
            r = requests.get("http://{0}/".format(s))
        except requests.exceptions.RequestException:
            message["description"] = 'Server unreachable.'
            responses.append(message)
            code = 404
    if len(responses) > 0:
        return Response(response=json.dumps(responses), status=code, mimetype='application/json')
    else:
        return None


@app.route('/commit', methods=['GET'])
def commit():
    global servers
    global operations
    global waiting_for_rollback
    rnd.shuffle(servers)

    if not waiting_for_rollback:
        fail = []
        bad_request = False
        for s in servers:
            rnd.shuffle(operations)
            bad_connection = False

            for op in operations:
                try:
                    op_json = op.get_json()
                    op_text = op.get_text()
                    r = requests.post("http://{0}/{1}".format(s, op_text), json=op_json)
                except:
                    bad_connection = True
                    message = {"server": s, "description": "Server unreachable."}
                    fail.append(message)
                if bad_connection:
                    break

                if r.status_code != 200:
                    op_json["operation"] = op_text
                    message = {"server": s, "description": r.json()["description"], "operation": op_json}
                    fail.append(message)
                    bad_request = True
                    break

        if not bad_request:
            operations = []
            return send_get_request("commit")
        else:
            waiting_for_rollback = True
            return Response(response=json.dumps(fail), status=400, mimetype='application/json')
    else:
        return Response(response=json.dumps({"description": "Inconsistency state! Do rollback before any other "
                                                            "operation."}), status=409, mimetype='application/json')


if __name__ == '__main__':
    app.run(host="0.0.0.0")
