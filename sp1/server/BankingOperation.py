import enum


class BankingOperation(object):

    def __init__(self, transaction_id, operation, amount):
        self.transaction_id = transaction_id
        self.operation = operation
        self.amount = amount

    def __repr__(self):
        if self.operation == Operation.credit:
            return "Transaction ID: {0} - CREDIT - amount: {1}".format(self.transaction_id, self.amount)
        else:
            return "Transaction ID: {0} - DEBIT - amount: {1}".format(self.transaction_id, self.amount)


class Operation(enum.Enum):
    credit = 0
    debit = 1
