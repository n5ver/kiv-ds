from flask import Flask, jsonify, request, Response, json
from BankingOperation import *

app = Flask(__name__)

account = 5000000
latest_consistency = account
operations = []


@app.route('/rollback', methods=['GET'])
def rollback():
    global account
    global latest_consistency
    global operations

    account = latest_consistency
    operations = []
    return jsonify({'description': 'OK'})


@app.route('/credit', methods=['POST'])
def credit():
    global latest_consistency
    global account
    latest_consistency = account
    return parse_operation(request, Operation.credit)


@app.route('/debit', methods=['POST'])
def debit():
    global latest_consistency
    global account
    latest_consistency = account
    return parse_operation(request, Operation.debit)


@app.route('/balance', methods=['GET'])
def balance():
    global account
    return jsonify({'balance': account})


@app.route('/commit', methods=['GET'])
def commit():
    update_account()
    global account
    return jsonify({'balance': account})


def check_id(transaction_id):
    global operations

    if transaction_id is not None:
        if transaction_id > -1:
            stopped = False
            for banking_operation in operations:
                if banking_operation.transaction_id == transaction_id:
                    stopped = True
                    break
            return not stopped

    return False


def parse_operation(req, banking_operation):
    if req.is_json:
        message = None
        transaction_id = None
        amount = None

        try:
            transaction_id = int(req.json["transaction_id"])
        except KeyError:
            message = {'description': "Bad request. Value transaction_id has to be present."}
        except:
            message = {'description': "Bad request. Value transaction_id has to be integer."}

        try:
            amount = int(req.json["amount"])
            if amount < 10000 or amount > 50000:
                message = {'description': "Bad request. Value amount has to be from 10000 to 50000."}
        except KeyError:
            message = {'description': "Bad request. Value amount has to be present."}
        except:
            message = {'description': "Bad request. Value amount has to be integer."}

        if not check_id(transaction_id):
            message = {'description': "Bad request. Transaction_id has to be unique since latest commit and it has to "
                                      "start from zero and be integer."}

        if message is None:
            global operations
            operations.append(BankingOperation(transaction_id, banking_operation, amount))
            return Response(response=json.dumps({'description': 'OK'}), status=200, mimetype='application/json')
        else:
            return Response(response=json.dumps(message), status=400, mimetype='application/json')
    else:
        message = {'description': "Bad request. Request has to be in JSON format."}
        return Response(response=json.dumps(message), status=400, mimetype='application/json')


def update_account():
    global account
    global operations
    global latest_consistency

    operations.sort(key=lambda x: x.transaction_id)
    latest_consistency = account
    for banking_operation in operations:
        if banking_operation.operation == Operation.credit:
            account += banking_operation.amount
        else:
            account -= banking_operation.amount

    operations = []


@app.route('/', methods=['GET'])
def test():
    return Response(response=json.dumps({'description': 'OK'}), status=200, mimetype='application/json')


if __name__ == '__main__':
    app.run(host="0.0.0.0")
